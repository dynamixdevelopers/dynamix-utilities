package org.ambientdynamix.external.profiling;

/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Struct-like class for performance statistics.
 * 
 * @author Darren Carlson
 */
public class PerformanceStats {
    private static SimpleDateFormat DateFormat = new SimpleDateFormat("HH:mm:ss:SSS");
    public Date sampleTime;
    public long millsSinceSamplingStarted;
    public int processid;
    public String processName;
    public float totalCpuLoad;
    public float totalUserCpuLoad;
    public float totalSystemCpuLoad;
    public float totalIoWaitTime;
    public float totalIrqTime;
    public float totalSoftIrqTime;
    public float processCpuLoad;
    public float processUserCpuLoad;
    public float processSystemCpuLoad;
    public float processIoWaitTime;
    public float processIrqTime;
    public float processSoftIrqTime;
    public int processMajorFaults;
    public int processMinorFaults;
    public MemorySample memUsage;
    
    @Override
    public boolean equals(Object in) {
        if(this == in) return true;
        if (in instanceof PerformanceStats){
            PerformanceStats check = (PerformanceStats) in;
            return this.processid == check.processid;
        }
        
        return false;
    }
    
    // HashCode Example: http://www.javafaq.nu/java-example-code-175.html
    @Override
    public int hashCode() {
	int result = 17;
	result = 31 * result + this.processid;
	return result;
    }
    
    public static class MemorySample {
	public float TotalHeapInBytes;
	public float HeapUsedInBytes;
    }

    @Override
    public String toString() {
	StringBuilder b = new StringBuilder();
//	b.append("Time: " + DateFormat.format(sampleTime));
//	b.append(" | ");
	b.append("Id: " + processid);
	b.append(" | ");
	b.append("Name: " + processName);
	b.append(" | ");
	b.append("CPU: " + processCpuLoad);
	b.append(" | ");
	b.append("CPU (User): " + processUserCpuLoad);
	b.append(" | ");
	b.append("CPU (System): " + processSystemCpuLoad);
	b.append(" | ");
	b.append("Rel Mils: " + millsSinceSamplingStarted);
	b.append(" | ");
	b.append("Total CPU: " + totalCpuLoad);
	b.append(" | ");
	b.append("Total CPU (User): " + totalUserCpuLoad);
	b.append(" | ");
	b.append("Total CPU (System): " + totalSystemCpuLoad);
	
	return b.toString();
    }

//    public String toCsv() {
//	StringBuilder b = new StringBuilder();
//	b.append(DateFormat.format(sampleTime));
//	b.append(",");
//	b.append("Total CPU");
//	b.append(",");
//	b.append(totalCpuLoad);
//	b.append(",");
//	b.append(totalUserCpuLoad);
//	b.append(",");
//	b.append(totalSystemCpuLoad);
//	b.append(",");
//	b.append("Process Id: " + processid + "Process Name: " + processName);
//	b.append(",");
//	b.append(processCpuLoad);
//	b.append(",");
//	b.append(processUserCpuLoad);
//	b.append(",");
//	b.append(processSystemCpuLoad);
//	return b.toString();
//    }

    public String toCsv2() {
	DecimalFormat dec = new DecimalFormat();
	dec.setMaximumFractionDigits(2);
	StringBuilder b = new StringBuilder();
	b.append(processName);
	b.append(",");
	b.append(millsSinceSamplingStarted);
	b.append(",");
	b.append(totalCpuLoad);
	b.append(",");
	b.append(totalUserCpuLoad);
	b.append(",");
	b.append(totalSystemCpuLoad);
	b.append(",");
	b.append(processCpuLoad);
	b.append(",");
	b.append(processUserCpuLoad);
	b.append(",");
	b.append(processSystemCpuLoad);
	b.append(",");
	b.append(dec.format(memUsage.TotalHeapInBytes));
	b.append(",");
	b.append(dec.format(memUsage.HeapUsedInBytes));
	b.append(",");
	return b.toString();
    }
}