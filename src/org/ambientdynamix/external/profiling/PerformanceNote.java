package org.ambientdynamix.external.profiling;
/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple struct-like note that can be included in performance profiling results.
 * @author Darren Carlson
 *
 */
public class PerformanceNote {
    // Private data
    private static SimpleDateFormat DateFormat = new SimpleDateFormat("HH:mm:ss:SSS");
    public Date timeStamp;
    public long millsSinceSamplingStarted;
    public String noteMessage;
    
    @Override
    public String toString() {
        return DateFormat.format(timeStamp) + " (" + millsSinceSamplingStarted + "): " + noteMessage;
    }
}
