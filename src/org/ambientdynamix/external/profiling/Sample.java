package org.ambientdynamix.external.profiling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sample {
    private Map<String, List<PerformanceStats>> statMap = new HashMap<String, List<PerformanceStats>>();

    public void addPerformanceStats(PerformanceStats stats) {
	if (statMap.containsKey(stats.processName)) {
	    List<PerformanceStats> list = statMap.get(stats.processName);
	    list.add(stats);
	}
	else {
	    List<PerformanceStats> list = new ArrayList<PerformanceStats>();
	    list.add(stats);
	    statMap.put(stats.processName, list);
	}
    }

    public Map<String, List<PerformanceStats>> getStatMap() {
	return this.statMap;
    }
}
