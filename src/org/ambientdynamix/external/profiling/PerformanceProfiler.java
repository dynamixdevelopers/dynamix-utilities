package org.ambientdynamix.external.profiling;

/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.ambientdynamix.external.profiling.PerformanceStats.MemorySample;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.util.Log;

/**
 * Threaded performance profiler that collects CPU and heap statistics.
 * 
 * @author Darren Carlson
 */
public class PerformanceProfiler {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private List<Sample> samples = new Vector<Sample>();
	private List<PerformanceNote> notes = new Vector<PerformanceNote>();
	private PerformanceProfilerEnginer2 statEngine = new PerformanceProfilerEnginer2(false);
	private ProfilerThread profilerThread;
	private Thread startDelayThread;
	private Thread stopDelayThread;
	private boolean done = true;
	private boolean profiling;
	private boolean stopping;
	private long samplePeriodMills;
	private long samplingDelay;
	private Date samplingStart;
	private Date samplingStop;
	private List<IPerformanceProfilerListener> listeners = new Vector<IPerformanceProfilerListener>();
	private Context context;
	private ActivityManager activityManager;
	private boolean useAveraging;
	/*
	 * Map is Hz to Test (each row is different Hz value). Use LinkedHashMap to maintain insertion order.
	 */
	private Map<Integer, List<Test>> testMap = new LinkedHashMap<Integer, List<Test>>();

	class Test {
		long Hz;
		float loadKbytes;
		double cpuAverage;
		double cpuAverageSd;
		Map<String, Double> processCpuAverage = new HashMap<String, Double>();
		Map<String, Double> processCpuSd = new HashMap<String, Double>();
		Map<String, Double> processHeapAverage = new HashMap<String, Double>();
		Map<String, Double> processHeapAverageSd = new HashMap<String, Double>();
	}

	public interface IPerformanceProfilerListener {
		void onProfilingStarting();

		void onProfilingStarted();

		void onProfilingStopping();

		void onProfilingStopped();
	}

	public void startAveraging() {
		this.useAveraging = true;
	}

	public void stopAveraging() {
		this.useAveraging = false;
	}

	public void clearAveragingTests() {
		this.testMap.clear();
	}

	public boolean addListener(IPerformanceProfilerListener listener) {
		synchronized (listeners) {
			if (listener != null && !listeners.contains(listener))
				return listeners.add(listener);
			else
				Log.w(TAG, "Listener already added or null: " + listener);
			return false;
		}
	}

	public boolean removeListener(IPerformanceProfilerListener listener) {
		synchronized (listeners) {
			return listeners.remove(listener);
		}
	}

	public PerformanceProfiler(Context context, long samplePeriodMills) {
		this.context = context;
		this.activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		statEngine.init();
		setSamplingDelay();
		setsamplePeriodMills(samplePeriodMills);
		Log.i(TAG, "Created with sampling period: " + this.samplePeriodMills);
	}

	private void setSamplingDelay() {
		samples.clear();
		int TEST_NUM = 5;
		long testDelay = 0;
		statEngine.init();
		for (int i = 0; i < TEST_NUM; i++) {
			Date before = new Date();
			Set<String> sampleFilter = new HashSet<String>();
			sampleFilter.add("system_server");
			samples.add(getPerformanceSample(before, sampleFilter, null));
			Date after = new Date();
			Log.i(TAG, "Sample delay: " + (after.getTime() - before.getTime()));
			testDelay += (after.getTime() - before.getTime());
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
			}
		}
		samplingDelay = testDelay / TEST_NUM;
		samples.clear();
		Log.i(TAG, "Discovered sampling delay of: " + samplingDelay + "ms");
	}

	public synchronized void addPerformanceNote(String noteMessage) {
		if (isProfiling()) {
			PerformanceNote note = new PerformanceNote();
			note.timeStamp = new Date();
			note.noteMessage = noteMessage;
			note.millsSinceSamplingStarted = note.timeStamp.getTime() - samplingStart.getTime();
			notes.add(note);
		} else
			Log.w(TAG, "Cannot add Performance Note if not profiling!");
	}

	public List<PerformanceNote> getPerformanceNotes() {
		return this.notes;
	}

	public boolean isProfiling() {
		return profiling;
	}

	public long getSamplePeriodMills() {
		return this.samplePeriodMills;
	}

	public void setsamplePeriodMills(long samplePeriodMills) {
		if (samplingDelay > samplePeriodMills) {
			this.samplePeriodMills = 1;
			Log.w(TAG, "Sampling delay is longer than requested sampling period. Sampling set to: " + samplePeriodMills
					+ "ms");
		} else {
			long originalSamplePeriodMills = samplePeriodMills;
			this.samplePeriodMills = samplePeriodMills - samplingDelay;
			Log.i(TAG, "Requested sampling rate of " + originalSamplePeriodMills + "ms has been adjusted to "
					+ this.samplePeriodMills + "ms" + " due to a sampling delay of " + samplingDelay + "ms");
		}
	}

	public void clear() {
		this.samples.clear();
		this.notes.clear();
	}

	public List<Sample> getPerformanceSamples() {
		return this.samples;
	}

	public synchronized boolean startProfiling(final int startDelayMills, Set<String> filters,
			Map<String, Float> scalerMap) {
		Log.i(TAG, "Starting profiling with a start delay of " + startDelayMills + "ms and a sampling rate of "
				+ samplePeriodMills + "ms");
		if (isProfiling()) {
			Log.w(TAG, "Already profiling!");
			return false;
		} else {
			profiling = true;
			done = false;
			stopping = false;
			synchronized (listeners) {
				for (IPerformanceProfilerListener listener : listeners) {
					listener.onProfilingStarting();
				}
			}
			profilerThread = new ProfilerThread(filters, scalerMap);
			profilerThread.setDaemon(true);
			profilerThread.setPriority(Thread.MIN_PRIORITY);
			startDelayThread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						if (startDelayMills > 0)
							Thread.sleep(startDelayMills);
						if (isProfiling()) {
							profilerThread.start();
						}
					} catch (InterruptedException e) {
					}
				}
			});
			startDelayThread.setDaemon(true);
			startDelayThread.start();
			return true;
		}
	}

	public synchronized boolean stopProfiling() {
		if (!isProfiling()) {
			Log.w(TAG, "Not profiling!");
			return false;
		} else {
			stopping = true;
			synchronized (listeners) {
				for (IPerformanceProfilerListener listener : listeners) {
					listener.onProfilingStopping();
				}
				cancelThreads();
				return true;
			}
		}
	}

	public synchronized boolean stopProfiling(final long stopDelayMills) {
		Log.i(TAG, "Stopping profiling in " + stopDelayMills + "ms");
		if (!isProfiling()) {
			Log.w(TAG, "Not profiling!");
			return false;
		} else {
			if (!stopping) {
				stopping = true;
				synchronized (listeners) {
					for (IPerformanceProfilerListener listener : listeners) {
						listener.onProfilingStopping();
					}
				}
				stopDelayThread = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							if (stopDelayMills > 0)
								Thread.sleep(stopDelayMills);
							cancelThreads();
						} catch (InterruptedException e) {
						}
					}
				});
				stopDelayThread.setDaemon(true);
				stopDelayThread.start();
			} else
				Log.w(TAG, "Already stopping... please wait");
			return true;
		}
	}

	public void writePerformanceNotesToFile(String path) {
		try {
			Log.i(TAG, "Writing " + notes.size() + " notes to file: " + path);
			FileWriter outFile = new FileWriter(path);
			PrintWriter out = new PrintWriter(outFile);
			for (PerformanceNote note : notes) {
				out.println(note.toString());
			}
			out.close();
		} catch (IOException e) {
			Log.w(TAG, "Could not write performance notes to file: " + e);
		}
	}

	public boolean writeStatsToFile(String path) {
		try {
			Log.i(TAG, "Writing " + samples.size() + " samples to file: " + path);
			FileWriter outFile = new FileWriter(path);
			PrintWriter out = new PrintWriter(outFile);
			// Write out each sample on a single CSV line
			for (Sample stats : samples) {
				for (List<PerformanceStats> processStats : stats.getStatMap().values()) {
					for (PerformanceStats individual : processStats)
						out.print(individual.toCsv2());
				}
				out.println();
			}
			out.close();
			return true;
		} catch (IOException e) {
			Log.w(TAG, "Could not write stats to file: " + e);
		}
		return false;
	}

	public void writeMeansToFile(String path, boolean append, float loadKbytes, long hz) {
		try {
			FileWriter outFile = new FileWriter(path, append);
			PrintWriter out = new PrintWriter(outFile);
			// Collect stats for each process in each sample
			Map<String, List<Float>> cpuMap = new HashMap<String, List<Float>>();
			Map<String, List<Float>> heapMap = new HashMap<String, List<Float>>();
			for (Sample sample : samples) {
				boolean gotCpu = false;
				for (List<PerformanceStats> processStats : sample.getStatMap().values()) {
					for (PerformanceStats individual : processStats) {
						// Collect total CPU (once per sample)
						if (!gotCpu) {
							gotCpu = true;
							if (cpuMap.containsKey("total.cpu")) {
								List<Float> cpuSamples = cpuMap.get("total.cpu");
								cpuSamples.add(individual.totalCpuLoad);
							} else {
								List<Float> cpuSamples = new ArrayList<Float>();
								cpuSamples.add(individual.processCpuLoad);
								cpuMap.put("total.cpu", cpuSamples);
							}
						}
						// Collect process CPU
						if (cpuMap.containsKey(individual.processName)) {
							List<Float> cpuSamples = cpuMap.get(individual.processName);
							cpuSamples.add(individual.processCpuLoad);
						} else {
							List<Float> cpuSamples = new ArrayList<Float>();
							cpuSamples.add(individual.processCpuLoad);
							cpuMap.put(individual.processName, cpuSamples);
						}
						// Collect process heap
						if (heapMap.containsKey(individual.processName)) {
							List<Float> heapSamples = heapMap.get(individual.processName);
							heapSamples.add(individual.memUsage.HeapUsedInBytes);
						} else {
							List<Float> heapSamples = new ArrayList<Float>();
							heapSamples.add(individual.memUsage.HeapUsedInBytes);
							heapMap.put(individual.processName, heapSamples);
						}
					}
				}
			}
			// Construct CSV for data: name, load, hz, cpu_mean, cpu_sd, heap_mean, heap_sd | (repeat for other
			// processes on same line)
			// First pull out the total.cpu samples and figure our their mean and sd values
			List<Float> cpuSamples = cpuMap.remove("total.cpu");
			double totalCpuMean = findMean(cpuSamples);
			double totalCpuSd = findStandardDeviation(totalCpuMean, cpuSamples);
			DecimalFormat f = new DecimalFormat("#.##");
			// Now, each process in the cpuMap should have a corresponding value in the heapMap (or we've done something
			// wrong)
			for (String process : cpuMap.keySet()) {
				// Log.i(TAG, "Writing process: " + process);
				StringBuilder b = new StringBuilder();
				b.append(process);
				b.append(",");
				b.append(loadKbytes);
				b.append(",");
				b.append(hz);
				b.append(",");
				b.append(f.format(totalCpuMean));
				b.append(",");
				b.append(f.format(totalCpuSd));
				b.append(",");
				double processCpuMean = findMean(cpuMap.get(process));
				b.append(f.format(processCpuMean));
				b.append(",");
				b.append(f.format(findStandardDeviation(processCpuMean, cpuMap.get(process))));
				b.append(",");
				double heapMean = findMean(heapMap.get(process));
				b.append(f.format(heapMean));
				b.append(",");
				b.append(f.format(findStandardDeviation(heapMean, heapMap.get(process))));
				b.append(",");
				out.print(b.toString());
			}
			out.println();
			out.close();
		} catch (Exception e) {
			Log.w(TAG, "Could not write means to file: " + e);
		}
	}

	public void addTestToAveraging(float loadKbytes, int hz) {
		if (useAveraging) {
			Test t = new Test();
			t.Hz = hz;
			t.loadKbytes = loadKbytes;
			Map<String, List<Float>> cpuMap = new HashMap<String, List<Float>>();
			Map<String, List<Float>> heapMap = new HashMap<String, List<Float>>();
			List<Float> cpuTally = new ArrayList<Float>();
			// Calculate the averages for these samples and update the test
			for (Sample sample : samples) {
				boolean gotCpu = false;
				for (List<PerformanceStats> processStats : sample.getStatMap().values()) {
					for (PerformanceStats individual : processStats) {
						// Collect total CPU (once per sample)
						if (!gotCpu) {
							gotCpu = true;
							cpuTally.add(individual.totalCpuLoad);
						}
						// Collect process CPU
						if (cpuMap.containsKey(individual.processName)) {
							List<Float> cpuSamples = cpuMap.get(individual.processName);
							cpuSamples.add(individual.processCpuLoad);
						} else {
							List<Float> cpuSamples = new ArrayList<Float>();
							cpuSamples.add(individual.processCpuLoad);
							cpuMap.put(individual.processName, cpuSamples);
						}
						// Collect process heap
						if (heapMap.containsKey(individual.processName)) {
							List<Float> heapSamples = heapMap.get(individual.processName);
							heapSamples.add(individual.memUsage.HeapUsedInBytes);
						} else {
							List<Float> heapSamples = new ArrayList<Float>();
							heapSamples.add(individual.memUsage.HeapUsedInBytes);
							heapMap.put(individual.processName, heapSamples);
						}
					}
				}
				// Calculate the CPU average for this test
				t.cpuAverage = findMean(cpuTally);
				t.cpuAverageSd = findStandardDeviation(t.cpuAverage, cpuTally);
				// Calculate the process averages for this test
				for (String process : cpuMap.keySet()) {
					Double cpuAverage = findMean(cpuMap.get(process));
					Double cpuSd = findStandardDeviation(cpuAverage, cpuMap.get(process));
					Double heapAverage = findMean(heapMap.get(process));
					Double heapSd = findStandardDeviation(heapAverage, heapMap.get(process));
					t.processCpuAverage.put(process, cpuAverage);
					t.processCpuSd.put(process, cpuSd);
					t.processHeapAverage.put(process, heapAverage);
					t.processHeapAverageSd.put(process, heapSd);
				}
			}
			// Finally, add the test to the testMap
			if (testMap.containsKey(hz)) {
				testMap.get(hz).add(t);
			} else {
				List<Test> tests = new ArrayList<PerformanceProfiler.Test>();
				tests.add(t);
				testMap.put(hz, tests);
			}
		} else
			Log.w(TAG, "Call to addTestToAveraging when useAveraging false... ignoring");
	}

	public void writeAverages(String path, boolean append) {
		try {
			FileWriter outFile = new FileWriter(path, append);
			PrintWriter out = new PrintWriter(outFile);
			/*
			 * Write out each row, which is all tests mapped to each Hz value
			 */
			DecimalFormat f = new DecimalFormat("#.##");
			for (int hzRow : testMap.keySet()) {
				for (Test t : testMap.get(hzRow)) {
					for (String process : t.processCpuAverage.keySet()) {
						StringBuilder b = new StringBuilder();
						b.append(process);
						b.append(",");
						b.append(t.loadKbytes);
						b.append(",");
						b.append(t.Hz);
						b.append(",");
						b.append(f.format(t.cpuAverage));
						b.append(",");
						b.append(f.format(t.cpuAverageSd));
						b.append(",");
						b.append(f.format(t.processCpuAverage.get(process)));
						b.append(",");
						b.append(f.format(t.processCpuSd.get(process)));
						b.append(",");
						b.append(f.format(t.processHeapAverage.get(process)));
						b.append(",");
						b.append(f.format(t.processHeapAverageSd.get(process)));
						b.append(",");
						out.print(b.toString());
					}
				}
				out.println();
			}
			out.close();
		} catch (Exception e) {
			Log.w(TAG, "Could not write means to file: " + e);
		}
	}

	private void cancelThreads() {
		// Cancel any scheduled delayed starts
		if (startDelayThread != null && startDelayThread.isAlive()) {
			startDelayThread.interrupt();
			startDelayThread = null;
		}
		// Cancel any scheduled delayed stops
		if (stopDelayThread != null && stopDelayThread.isAlive()) {
			stopDelayThread.interrupt();
			stopDelayThread = null;
		}
		// Cancel profiling
		done = true;
		if (profilerThread != null && profilerThread.isAlive()) {
			profilerThread.interrupt();
			profilerThread = null;
		}
	}

	private class ProfilerThread extends Thread {
		private Map<String, Float> scalerMap;
		private Set<String> filters;

		public ProfilerThread(Set<String> filters, Map<String, Float> scalerMap) {
			this.filters = filters;
			this.scalerMap = scalerMap;
		}

		@Override
		public void run() {
			Log.i(TAG, "Performance profile thread has started");
			synchronized (listeners) {
				for (IPerformanceProfilerListener listener : listeners) {
					listener.onProfilingStarted();
				}
			}
			statEngine.update();
			samplingStart = new Date();
			while (!done) {
				try {
					Thread.sleep(samplePeriodMills);
					Sample s = getPerformanceSample(samplingStart, filters, scalerMap);
					samples.add(s);
				} catch (InterruptedException e) {
				}
			}
			samplingStop = new Date();
			Log.i(TAG, "Performance profile thread has stopped");
			profiling = false;
			synchronized (listeners) {
				for (IPerformanceProfilerListener listener : listeners) {
					listener.onProfilingStopped();
				}
			}
		}
	}

	private Sample getPerformanceSample(Date startMills, Set<String> filters, Map<String, Float> scalerMap) {
		Date startSample = new Date();
		Sample sample = statEngine.getCurrentState(filters, scalerMap);
		
		Date endSample = new Date();
		long samplingMiddlePoint = (endSample.getTime() - startSample.getTime()) / 2;
		// Update heap for each sample
		for (List<PerformanceStats> tmp : sample.getStatMap().values())
			for (PerformanceStats stat : tmp) {
				stat.millsSinceSamplingStarted = (startSample.getTime() - startMills.getTime() + samplingMiddlePoint);
				MemorySample memUsage = new MemorySample();
				memUsage.TotalHeapInBytes = Runtime.getRuntime().maxMemory() / 1048576;
				android.os.Debug.MemoryInfo[] memInfo = activityManager
						.getProcessMemoryInfo(new int[] { stat.processid });
				memUsage.HeapUsedInBytes = (memInfo[0].getTotalPss() * 1024) / 1048576;
				stat.memUsage = memUsage;
			}
		return sample;
	}

	private double findMean(List<Float> array) {
		double total = 0;
		for (Float f : array)
			total += f;
		double mean = total / array.size();
		return mean;
	}

	private double findStandardDeviation(double mean, List<Float> array) {
		// double mean = findMean(array);
		double d1 = 0;
		double d2 = 0;
		for (Float f : array) {
			d2 = (mean - f) * (mean - f);
			d1 = d2 + d1;
		}
		return Math.sqrt((d1 / (array.size() + 1)));
	}
}
