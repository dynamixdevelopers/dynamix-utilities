package org.ambientdynamix.apps.profiler;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ambientdynamix.external.profiling.PerformanceProfiler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BroadcastHandler extends BroadcastReceiver {
	private final static String TAG = BroadcastHandler.class.getSimpleName();

	@Override
	public void onReceive(Context c, Intent intent) {
		PerformanceProfiler profiler = PerformanceProfilerActivity.profiler;
		Log.i("BroadcastHandler", "New Intent with action: " + intent.getAction());
		if (intent.getAction().equalsIgnoreCase("org.ambientdynamix.apps.profiler.START_PROFILING")) {
			if (!profiler.isProfiling()) {
				PerformanceProfilerActivity.testName = intent.getStringExtra("testName");
				PerformanceProfilerActivity.loadKbytes = intent.getFloatExtra("loadKbytes", 0);
				PerformanceProfilerActivity.rateHz = intent.getIntExtra("rateHz", 0);
				Map<String, Float> scalerMap = (Map<String, Float>) intent.getSerializableExtra("scalerMap");
				Set<String> filterSet = new HashSet<String>();
				List<String> filters = intent.getStringArrayListExtra("filters");
				filterSet.addAll(filters);
				if (intent.getBooleanExtra("runLoader", false)) {
					Log.i(TAG, "Running load test in parallel");
					PerformanceProfilerActivity.startLoadThread();
				}
				profiler.setsamplePeriodMills(intent.getLongExtra("samplePeriodMills", 500));
				profiler.startProfiling(intent.getIntExtra("startDelay", 0), filterSet, scalerMap);
				Log.i("BroadcastHandler", "Starting profiler with test: " + PerformanceProfilerActivity.testName);
			} else
				Log.w(TAG, "Profiler already started");
		}
		if (intent.getAction().equalsIgnoreCase("org.ambientdynamix.apps.profiler.STOP_PROFILING")) {
			int stopMills = intent.getIntExtra("stopMills", 0);
			profiler.stopProfiling(stopMills);
			PerformanceProfilerActivity.stopLoadThread();
		}
		if (intent.getAction().equalsIgnoreCase("org.ambientdynamix.apps.profiler.START_AVERAGING")) {
			profiler.clearAveragingTests();
			profiler.startAveraging();
		}
		if (intent.getAction().equalsIgnoreCase("org.ambientdynamix.apps.profiler.WRITE_AVERAGING")) {
			profiler.writeAverages(intent.getStringExtra("meansFileName"), false);
			profiler.stopAveraging();
			profiler.clearAveragingTests();
		}
	}
}
