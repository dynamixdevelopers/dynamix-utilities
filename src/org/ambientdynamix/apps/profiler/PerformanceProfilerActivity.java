package org.ambientdynamix.apps.profiler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ambientdynamix.external.profiling.PerformanceProfiler;
import org.ambientdynamix.external.profiling.PerformanceProfiler.IPerformanceProfilerListener;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PerformanceProfilerActivity extends Activity implements IPerformanceProfilerListener {
	private final static String TAG = PerformanceProfilerActivity.class.getSimpleName();
	static PerformanceProfiler profiler;
	static String testName;
	static String meansFileName;
	static float loadKbytes;
	static int rateHz;
	private Handler uiHandler = new Handler();
	private Context context;
	private static Thread loadThread;
	private static boolean killLoadThread;

	public void toast(final String message, final int duration) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, message, duration).show();
			}
		});
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		profiler = new PerformanceProfiler(this, 500);
		profiler.addListener(this);
		LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		TextView lblSampleRate = new TextView(this);
		lblSampleRate.setText("Profiler Ready");
		layout.addView(lblSampleRate);
		setContentView(layout);
	}

	public static synchronized boolean startLoadThread() {
		if (loadThread == null) {
			killLoadThread = false;
			loadThread = new Thread(new Runnable() {
				@Override
				public void run() {
					// Naive prime number generator
					List<Integer> primes = new ArrayList<Integer>();
					// start from 2
					OUTERLOOP: for (int i = 2; i <= Integer.MAX_VALUE; i++) {
						if (killLoadThread)
							break;
						// try to divide it by all known primes
						for (Integer p : primes)
							if (i % p == 0)
								continue OUTERLOOP; // i is not prime
						// i is prime
						primes.add(i);
					}
					loadThread = null;
				}
			});
			loadThread.setDaemon(true);
			loadThread.start();
		} else
			Log.w(TAG, "Load thread already running");
		return false;
	}

	public static synchronized void stopLoadThread() {
		killLoadThread = true;
	}

	@Override
	public void onProfilingStarting() {
		Log.i(TAG, "onProfilingStarting");
	}

	@Override
	public void onProfilingStarted() {
		Log.i(TAG, "onProfilingStarted");
		// toast("Sampling Started!", 5000);
	}

	@Override
	public void onProfilingStopping() {
		Log.i(TAG, "onProfilingStopping");
	}

	@Override
	public void onProfilingStopped() {
		Log.i(TAG, "onProfilingStopped");
		toast("Sampling Stopped!", 5000);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
		Date now = new Date();
		if (profiler.getPerformanceSamples().size() > 0) {
			profiler.writeStatsToFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"
					+ dateFormat.format(now) + " - " + testName);
			profiler.addTestToAveraging(loadKbytes, rateHz);
			System.gc();
		}
		if (profiler.getPerformanceNotes().size() > 0)
			profiler.writePerformanceNotesToFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"
					+ dateFormat.format(now) + "-notes.txt");
		profiler.clear();
	}
}